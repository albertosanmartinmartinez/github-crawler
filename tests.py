
"""
"""

import pytest

from github_crawler import GithubCrawler


def test_init_with_file():

    gc = GithubCrawler('input_ex_1.json')
    gc.build_paths()

    assert gc.file_name == 'input_ex_1.json'


def test_init_without_file():

    with pytest.raises(ValueError):
        gc = GithubCrawler()


def test_read_input_file_not_exist():

    gc = GithubCrawler('file_not_exist.json')
    gc.build_paths()

    assert gc.read_input() == -1


def test_read_input_keywords_not_exist():

    gc = GithubCrawler('test_keywords_not_exist.json')
    gc.build_paths()

    assert gc.read_input() == -1


def test_read_input_test_keywords_is_void():

    gc = GithubCrawler('test_keywords_is_void.json')
    gc.build_paths()

    assert gc.read_input() == -1


def test_read_input_proxies_not_exist():

    gc = GithubCrawler('test_proxies_not_exist.json')
    gc.build_paths()

    assert gc.read_input() == -1


def test_read_input_proxies_is_void():

    gc = GithubCrawler('test_proxies_is_void.json')
    gc.build_paths()

    assert gc.read_input() == -1

def test_read_input_type_not_exist():

    gc = GithubCrawler('test_type_not_exist.json')
    gc.build_paths()

    assert gc.read_input() == -1

def test_read_input_type_not_match():

    gc = GithubCrawler('test_type_not_match.json')
    gc.build_paths()

    assert gc.read_input() == -1


def test_build_url_ok_url():

    gc = GithubCrawler('input_ex_1.json')
    gc.build_paths()
    gc.read_input()
    gc.build_url()

    assert gc.url_resquest == 'https://github.com/search?type=Repositories&q=%5B%27java%27%2C+%27spring%27%5D'

# request status codes

# test_get_repositories_list ...
# test_get_repository_detail ...








#
