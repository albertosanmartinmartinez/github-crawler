
Github Crawler

Run this command cli to install project dependencies
$ pip install -r requirements.txt

Run this command cli to execute the script
$ python main.py

Run this command cli to execute the tests
$ pytest tests.py
