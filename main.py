
import argparse

from github_crawler import GithubCrawler

if __name__ == '__main__':
    """
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', type=str, help='File as input for crawler', required=True)

    args = parser.parse_args()

    gc = GithubCrawler(args.file)
    gc.run()




#
