

"""
input: Read json file with a list of search keywords, a list of available proxies and the github search type
output: Write a json file with list of dictionaries containing url, author, and language stats from each repository
"""

import os
import json
import bs4
import datetime
import random

from urllib.parse import urlencode, quote_plus
from urllib.request import urlopen, Request

DIR_PATH = os.path.dirname(os.path.abspath(__file__))
INPUT_PATH = os.path.join(DIR_PATH, 'inputs')
OUTPUT_PATH = os.path.join(DIR_PATH, 'outputs')
TMP_PATH = os.path.join(DIR_PATH, 'tmp')

BASE_URL = 'https://github.com'


class GithubCrawler:
    """
    """

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
    }

    result = []
    types = ["Repositories", "Wikis", "Issues"]
    repositories = []

    def __init__(self, file_name=None):
        """
        """

        if file_name is None:
            raise ValueError

        self.file_name = file_name

    def run(self):
        """
        """

        self.build_paths()
        self.read_input()
        self.build_url()
        self.get_repositories_list()
        self.get_repository_detail()
        self.write_output()

    def build_paths(self):
        """
        method to prepare input/output/tmp paths
        """

        self.input_file_path = os.path.join(INPUT_PATH, self.file_name)
        datetime_str = datetime.datetime.now().strftime("%d-%m-%Y-%H-%M-%S")
        self.tmp_file_path = os.path.join(TMP_PATH, 'tmp-{}{}'.format(datetime_str, '.html'))
        self.output_file_path = os.path.join(OUTPUT_PATH, 'output-{}{}'.format(datetime_str, '.json'))

    def read_input(self):
        """
        method to read input file and prepare the info
        """

        try:
            json_file = open(self.input_file_path, 'r')
            self.json_input = json.load(json_file)
            json_file.close()
        except Exception as e:
            print("Error reading input file")
            print(e)
            return -1

        self.proxies = self.json_input.get('proxies', None)
        if self.proxies is None or len(self.proxies) <= 0:
            print("Error with proxies")
            return -1
        del self.json_input['proxies']

        keywords = self.json_input.get('keywords', None)
        if keywords is None or len(keywords) <= 0:
            print("Error with keywords")
            return -1
        self.json_input['q'] = keywords
        del self.json_input['keywords']

        type = self.json_input.get('type', None)
        if type is None or type not in self.types:
            print("Error with type")
            return -1

    def build_url(self):
        """
        method to build the url encoding and parsing the query parmas
        """

        print('build_url method')

        if 'q' in self.json_input.keys() and len(self.json_input['q']) > 0:
            self.url_resquest = BASE_URL + "/search?" + urlencode(self.json_input, quote_via=quote_plus)
        else:
            print("Error building url")
            return -1

    def get_repositories_list(self):
        """
        method to get urls from github search result
        """

        print('get_repositories_list method')

        request = Request(
            self.url_resquest,
            data=None,
            headers=self.headers
        )

        random_proxy_index = random.randint(0,len(self.proxies)-1)
        proxy = self.proxies[random_proxy_index]
        request.set_proxy(proxy, 'https')

        try:
            response = urlopen(request)
        except Exception as e:
            print("Error making the request")
            print(e)
            return -1

        self.content = response.read().decode(response.info().get_content_charset())

        #html_file = open(os.path.join(TMP_PATH, 'repo_list.html'), 'r')
        #soup = bs4.BeautifulSoup(html_file.read(), 'html.parser')
        soup = bs4.BeautifulSoup(self.content, 'html.parser')
        #html_file.close()

        result_items = soup.find_all("li", {"class": "repo-list-item"})

        for result_item in result_items:
            self.repositories.append(result_item.find("a", {"class": "v-align-middle"}).get('href'))

    def get_repository_detail(self):
        """
        method to get repository url and crawl the detail information
        """

        for repository in self.repositories:

            repo_detail = {}

            repo_url = BASE_URL + repository

            repo_detail["url"] = repo_url
            repo_detail["extra"] = {}

            request = Request(
                repo_url,
                data=None,
                headers=self.headers
            )

            random_proxy_index = random.randint(0,len(self.proxies)-1)
            proxy = self.proxies[random_proxy_index]
            request.set_proxy(proxy, 'https')

            try:
                response = urlopen(request)
            except Exception as e:
                print("Error making the request")
                print(e)
                return -1

            repository_content = response.read().decode(response.info().get_content_charset())

            #html_file = open(os.path.join(TMP_PATH, 'repo_detail.html'), 'r')
            #soup = bs4.BeautifulSoup(html_file.read(), 'html.parser')
            soup = bs4.BeautifulSoup(repository_content, 'html.parser')
            #html_file.close()

            repo_detail["extra"]["owner"] = soup.find(attrs={"rel": "author"}).text
            repo_detail["extra"]["language_stats"] = {}

            repository_languages = soup.find("div", {"class": "Layout-sidebar"}).find("ul", {"class": "list-style-none"})

            for repository_language in repository_languages.find_all("li", {"class": "d-inline"}):
                repository_language_dict = repository_language.find_all("span")
                repo_detail["extra"]["language_stats"][repository_language_dict[0].text] = float(repository_language_dict[1].text.split('%')[0])

            self.result.append(repo_detail)

    def write_output(self):
        """
        method to write result to json file
        """


        try:
            json_file = open(self.output_file_path, 'w')
            json.dump(self.result, json_file)
            json_file.close()
        except Exception as e:
            print(e)
            return -1

        """
        try:
            html_file = open(self.tmp_file_path, 'w')
            html_file.write(self.content)
            html_file.close()
        except Exception as e:
            print(e)
            return -1
        """





#
